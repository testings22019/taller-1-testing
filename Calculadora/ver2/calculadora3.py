from tkinter import *
from operacion import ClaseOperacion

ventana=Tk()
ventana.title("Calculadora")
ventana.geometry("992x600")
ventana.configure(background="SkyBlue1")
color_boton=("gray88")
color_boton1=("gray95")

ancho_boton=11
alto_boton=3

obj = ClaseOperacion()
obj.clear()

#####################################################################################################
#fila 6 Boton Numericos color1

Boton0=Button(ventana,text="0",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(0)).place(x=107,y=480)
BotonC=Button(ventana,text="π",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("pi")).place(x=17,y=480)
BotonE=Button(ventana,text="e",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("e")).place(x=17,y=545)
BotonEn=Button(ventana,text="n√",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("**1/")).place(x=107,y=545)

Boton0=Button(ventana,text="0",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(0)).place(x=107,y=480) 
BotonC=Button(ventana,text="π",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("pi")).place(x=17,y=480)
BotonE=Button(ventana,text="e",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("e")).place(x=17,y=545)
BotonEn=Button(ventana,text="n√",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("**(1/")).place(x=107,y=545) #parentesis agregado

BotonCua=Button(ventana,text="n²",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("**2")).place(x=197,y=545)
BotonComa=Button(ventana,text=",",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(".")).place(x=197,y=480)
#fila 5 Boton Numericos color1
Boton1=Button(ventana,text="1",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(1)).place(x=17,y=420)
Boton2=Button(ventana,text="2",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(2)).place(x=107,y=420)
Boton3=Button(ventana,text="3",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(3)).place(x=197,y=420)
#fila 4 Boton Numericos color1
Boton4=Button(ventana,text="4",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(4)).place(x=17,y=360)
Boton5=Button(ventana,text="5",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(5)).place(x=107,y=360)
Boton6=Button(ventana,text="6",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(6)).place(x=197,y=360)
#fila 3 Boton Numericos color1
Boton7=Button(ventana,text="7",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(7)).place(x=17,y=300)
Boton8=Button(ventana,text="8",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(8)).place(x=107,y=300)
Boton9=Button(ventana,text="9",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(9)).place(x=197,y=300)
#####################################################################################################

#####################################################################################################

#Columna Der

BotonSqrt=Button(ventana,text="√",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("sqrt(")).place(x=287,y=180)
BotonDiv=Button(ventana,text="/",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("/")).place(x=287,y=240)
BotonMulti=Button(ventana,text="*",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("*")).place(x=287,y=300)
BotonResta=Button(ventana,text="-",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("-")).place(x=287,y=360)
BotonSuma=Button(ventana,text="+",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("+")).place(x=287,y=420)
#####################################################################################################

#Boton Resultado
BotonResul=Button(ventana,text="=",bg=color_boton,width=ancho_boton,height=alto_boton,command=obj.operacion).place(x=287,y=480)

#Botón Decimal
BotonDecimal=Button(ventana,text="DEC",bg=color_boton,width=ancho_boton,height=alto_boton,command=obj.btn_dec).place(x=287,y=545)


#####################################################################################################
#fila 2 Boton Numericos color1
BotonParen1=Button(ventana,text="(",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("(")).place(x=17,y=240)
BotonParen2=Button(ventana,text=")",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click(")")).place(x=107,y=240)
BotonC=Button(ventana,text="C",bg=color_boton,width=ancho_boton,height=alto_boton,command=obj.clear).place(x=197,y=240)
#fila 1 Boton Numericos color1
BotonResto=Button(ventana,text="%",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("/100")).place(x=17,y=180)
Botonln=Button(ventana,text="ln",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("log10(")).place(x=107,y=180) #correcion log en base 10
BotonExp=Button(ventana,text="EXP",bg=color_boton,width=ancho_boton,height=alto_boton,command=lambda:obj.btn_click("**")).place(x=197,y=180)
#####################################################################################################

#Logintud
Botonpul=Button(ventana,text="pul->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.longitud("pul")).place(x=410,y=180)
Botonmcm=Button(ventana,text="mm->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.longitud("mm")).place(x=520,y=180)
Botonmcm=Button(ventana,text="cm->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.longitud("cm")).place(x=630,y=180)
Botonmcm=Button(ventana,text="m->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.longitud("m")).place(x=740,y=180)
Botonmcm=Button(ventana,text="km->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.longitud("km")).place(x=850,y=180)
#temperatura
Botoncel=Button(ventana,text="c->U.T.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.temperatura("c")).place(x=410,y=240)
Botonfar=Button(ventana,text="f->U.T.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.temperatura("f")).place(x=520,y=240)
Botonkel=Button(ventana,text="k->U.T.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.temperatura("k")).place(x=630,y=240)
#volumen
Botoncmv=Button(ventana,text="cm³->U.V.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.volumen("cm")).place(x=410,y=300)
Botonmv=Button(ventana,text="m³->U.V.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.volumen("m")).place(x=520,y=300)
Botonlt=Button(ventana,text="L->U.V.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.volumen("l")).place(x=630,y=300)
Botongal=Button(ventana,text="gal->U.V.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.volumen("gal")).place(x=740,y=300)
#peso
Botongr=Button(ventana,text="gr->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.peso("gr")).place(x=410,y=360)
Botonkg=Button(ventana,text="kg->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.peso("kg")).place(x=520,y=360)
Botonto=Button(ventana,text="ton->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.peso("ton")).place(x=630,y=360)
Botonlb=Button(ventana,text="Li->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.peso("lb")).place(x=740,y=360)
#presion
Botonpas=Button(ventana,text="Pascal->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.presion("pas")).place(x=410,y=420)
Botonatm=Button(ventana,text="Atmosfera->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.presion("atm")).place(x=520,y=420)
Botontor=Button(ventana,text="Torr->U.L.",bg=color_boton1,width=ancho_boton,height=alto_boton,command=lambda:obj.presion("tor")).place(x=630,y=420)

#Boton de memoria
Botonmem = Button(ventana,text="M",bg=color_boton,width=ancho_boton,height=alto_boton,command= obj.btn_mem).place(x=410,y=480)

Salida=Entry(ventana,font=('arial',20,'bold'),width=60,textvariable=obj.input_text,bd=20,insertwidth=4,bg="powder blue",justify="right").place(x=10,y=60)

ventana.mainloop()
