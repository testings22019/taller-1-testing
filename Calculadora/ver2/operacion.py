from tkinter import StringVar
from math import * # import faltante restablecido


lista = list()

def memoria(num):
    lista.append(num)
    for i in lista:
        if (i == 5):
            lista.reverse()
            lista.pop()
            lista.reverse()
    print(lista)

def mostrar():
    if (len(lista) == 0):
        return
    else:
        numero = lista.pop()
        return numero

def minimo (denom, nume):
    denom = int(denom)
    nume = int(nume)
    flag = True
    divisor = 2
    while(flag == True):
        if denom % divisor == 0 and nume % divisor == 0:
            denom = denom / divisor
            nume = nume / divisor
            divisor = 2
        elif ((nume > divisor and denom == divisor) or (nume < divisor and denom == divisor)):
            flag = False
        else:
            divisor += 1
    exp = str(nume) + "/" + str(denom)
    return exp

def to_str(num):
    aux = str(num)
    nume = ""
    count = 0
    if num >= 1:
        for i in aux:
            if i != ".":
                nume = nume + str(i)
                count += 1
        denom = str(1)
        for i in range(1,count):
            denom = denom + str(0)
        exp = minimo(denom, nume)
    else:
        for i in aux:
            count += 1
            if i != "." and i != "0":
                nume = nume + str(i)                
        denom = str(1)
        for i in range(1,count-1):
            denom = denom + str(0)
        exp = minimo(denom,nume)        
    return exp

class ClaseOperacion:
    def __init__(self):
        self.input_text=StringVar()
        self.operador = ""
    def btn_click(self,num):
        self.operador=self.operador+str(num)
        self.input_text.set(self.operador)
    def clear(self):
        self.operador=("")
        self.input_text.set("0")
    def btn_mem(self):
        imp = mostrar()
        self.input_text.set(imp)
    def btn_dec(self):
        try:
            num = self.input_text.get()
            num = float(num)
            imp = to_str(num)
            self.clear()
            self.input_text.set(imp)
            memoria(imp)
        except:
            self.clear()
            self.input_text.set("Error")
    def longitud(self, unid):
        self.operador=self.operador+str(unid)
        try:
            uni_1 = self.operador[-1]
            uni_2 = self.operador[-2]
            uni_3 = self.operador[-3]
            unidad = uni_2+uni_1
            unidad_pul = uni_3+uni_2+uni_1
        except:
            unidad_pul = ""
            unidad = self.operador[-1]

        if unidad_pul == "pul":
            val_pul = self.operador.replace('pul','')
            self.input_text.set(str(float(val_pul)*25.4)+" mm, "+str(float(val_pul)*2.54)+" cm, "+str(float(val_pul)/39.37)+" m, "+str(float(val_pul)/39370.079)+" km.")
        elif unidad == "mm":
            val_mm = self.operador.replace('mm','')
            self.input_text.set(str(float(val_mm)/25.4)+" pul, "+str(float(val_mm)/10)+" cm, "+str(float(val_mm)/1000)+" m, "+str(float(val_mm)/1000000)+" km.")
        elif unidad == "cm":
            val_cm = self.operador.replace('cm','')
            self.input_text.set(str(float(val_cm)/2.54)+" pul, "+str(float(val_cm)*10)+" mm, "+str(float(val_cm)/100)+" m, "+str(float(val_cm)/100000)+" km.")
        elif unidad == "km":
            val_km = self.operador.replace('km','')
            self.input_text.set(str(float(val_km)*39370.079)+" pul, "+str(float(val_km)*1000000)+" mm, "+str(float(val_km)*100000)+" cm, "+str(float(val_km)*1000)+" m.")
        else:
            val_m = self.operador.replace('m','')
            self.input_text.set(str(float(val_m)*39.37)+" pul, "+str(float(val_m)*1000)+" mm, "+str(float(val_m)*100)+" cm, "+str(float(val_m)/1000)+" km.")

    def temperatura(self, unid):
        self.operador=self.operador+str(unid)
        uni_1 = self.operador[-1]
        if uni_1 == "c":
            val_c = self.operador.replace('c','')
            self.input_text.set(str(float(val_c)*(9/5)+32)+" f, "+str(float(val_c)+273.15)+" k.")
        elif uni_1 == "f":
            val_f = self.operador.replace('f','')
            self.input_text.set(str((float(val_f)-32)*(5/9))+" c, "+str(float(val_f)-32*(5/9)+273.15)+" k.")
        elif uni_1 == "k":
            val_k = self.operador.replace('k','')
            self.input_text.set(str(float(val_k)-273.15)+" c, "+str((float(val_k)-273.15)*(9/5)+32)+" f.")
        else:
            self.clear()
            self.input_text.set("Error")

    def volumen(self, unid):
        self.operador=self.operador+str(unid)
        try:
            uni_1 = self.operador[-1]
            uni_2 = self.operador[-2]
            uni_3 = self.operador[-3]
            unidad = uni_2+uni_1
            unidad_gal = uni_3+uni_2+uni_1
        except:
            unidad_gal = ""
            unidad = self.operador[-1]

        if unidad == "cm":
            val_cm = self.operador.replace('cm','')
            self.input_text.set(str(float(val_cm)/1000000)+" m³, "+str(float(val_cm)/1000)+" l, "+str(float(val_cm)/3785.412)+" gal.")
        elif unidad == "l":
            val_l = self.operador.replace('l','')
            self.input_text.set(str(float(val_l)*1000)+" cm³, "+str(float(val_l)/1000)+" m³, "+str(float(val_l)/3.785)+" gal.")
        elif unidad_gal == "gal":
            val_gal = self.operador.replace('gal','')
            self.input_text.set(str(float(val_gal)*3785.412)+" cm³, "+str(float(val_gal)/264.172)+" m³, "+str(float(val_gal)*3.785)+" l.")
        else:
            val_m = self.operador.replace('m','')
            self.input_text.set(str(float(val_m)*1000000)+" cm³, "+str(float(val_m)*1000)+" l, "+str(float(val_m)*264.172)+" gal.")



    def peso(self, unid):
        self.operador=self.operador+str(unid)
        uni_1 = self.operador[-1]
        uni_2 = self.operador[-2]
        uni_3 = self.operador[-3]
        unidad = uni_2+uni_1
        unidad_lb = uni_3+uni_2+uni_1
        if unidad == "gr":
            val_gr = self.operador.replace('gr','')
            self.input_text.set(str(float(val_gr)/1000)+" kg, "+str(float(val_gr)/1000000)+" ton, "+str(float(val_gr)/453.592)+" lb.")
        elif unidad == "kg":
            val_kilo = self.operador.replace('kg','')
            self.input_text.set(str(float(val_kilo)*1000)+" gr, "+str(float(val_kilo)/1000)+" ton, "+str(float(val_kilo)*2.205)+" lb, ")
        elif unidad_lb == "ton":
            val_ton = self.operador.replace('ton','')
            self.input_text.set(str(float(val_ton)*1000000)+" gr, "+str(float(val_ton)*1000)+" kg, "+str(float(val_ton)*2204.623)+" lb, ")
        elif unidad == "lb":
            val_lb = self.operador.replace('lb','')
            self.input_text.set(str(float(val_lb)*453.592)+" gr, "+str(float(val_lb)/2.205)+" kg, "+str(float(val_lb)/2204.623)+" ton, ")
        else:
            self.clear()
            self.input_text.set("Error")
    



    def presion(self, unid):
        self.operador=self.operador+str(unid)
        uni_1 = self.operador[-1]
        uni_2 = self.operador[-2]
        uni_3 = self.operador[-3]
        unidad = uni_3+uni_2+uni_1
        if unidad == "pas":
            val_pas = self.operador.replace('pas','')
            self.input_text.set(str(float(val_pas)/101325)+" atm, "+str(float(val_pas)/133.322)+" tor, ")
        elif unidad == "atm":
            val_atm = self.operador.replace('atm','')
            self.input_text.set(str(float(val_atm)*101325)+" pas, "+str(float(val_atm)*760)+" tor, ")
     
        elif unidad == "tor":
            val_tor = self.operador.replace('tor','')
            self.input_text.set(str(float(val_tor)*133.322)+" pas, "+str(float(val_tor)/760)+" atm, ")

        else:
            self.clear()
            self.input_text.set("Error")
    



    def operacion(self):
        try:
            self.opera=str(eval(self.operador))
            memoria(self.opera)
        except:
            self.clear()
            self.opera=("Error")
        self.input_text.set(self.opera)
