from operaciones1 import Suma, Multiplicacion
from operaciones2 import Resta, Division
from operaciones3 import Porcentaje, Raiz, RaizEnecima, Potencia
from operaciones4 import DecimalToFraccion


def ComprobarInputFloat(a):
	try:
		return float(a)
	except:
		valor= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInputFloat(valor)


def ComprobarInput(a):
	try:
		return int(a)
	except:
		valor= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInput(valor)

def ComprobarInputDeno(a):
	try:
		if int(a)!=0:
			return int(a)
		elif int(a)==0:
			valor1= input("Error, el valor debe ser distinto a 0, Ingrese nuevamente el valor: ")
			return ComprobarInputDeno(valor1)
	except:
		valor2= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInputDeno(valor2)

def ComprobarInputNega(a):
	try:
		if int(a)>0:
			return int(a)
		elif int(a)<0:
			valor1= input("Error, el valor debe ser mayor a 0, Ingrese nuevamente el valor: ")
			return ComprobarInputDeno(valor1)
	except:
		valor2= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInputDeno(valor2)

def Rebobinar():
	print("Deseas realizar otra operacion: ")
	print("1)Si")
	print("2)No")
	Den = input("Ingrese opcion: ")
	ComDen = ComprobarInput(Den)

	if ComDen == 1:
		main()
	elif ComDen == 2:
		print("Adios....")
	else:
		print("Error en el valor ingresado")
		Rebobinar()

def main():
	print("-Operaciones-")
	print("1) Suma")
	print("2) Resta")
	print("3) Multiplicacion")
	print("4) Division")
	print("5) Potencia")
	print("6) Raiz cuadrada")
	print("7) RaizEnecima")
	print("8) Porcentaje")
	print("9) Decimal a Fraccion")
	opc = input("Ingrese el numero de la opcion de la operacion que desea realizar: ") 	

	if opc =="1":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInput(b)
		operacion=Suma(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc =="2":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInput(b)
		operacion=Resta(CompA,CompB)
		operacion.print_result()
		Rebobinar()                
	elif opc =="3":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInput(b)
		operacion=Multiplicacion(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc =="4":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInputDeno(b)
		operacion=Division(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc =="5":
		a= input("Ingrese base de la potencia: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese exponente de la potencia: ")
		CompB = ComprobarInput(b)
		operacion=Potencia(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc =="6":
		a= input("Ingrese el valor para obtener su raiz cuadrada: ")
		CompA = ComprobarInputNega(a)
		operacion=Raiz(CompA)
		operacion.print_result()
		Rebobinar()
	elif opc =="7":
		a= input("Ingrese radicando de la raiz: ")
		CompA = ComprobarInputNega(a)
		b= input("Ingrese indice de la raiz: ")
		CompB = ComprobarInputDeno(b)
		operacion=RaizEnecima(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc =="8":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInputDeno(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInputDeno(b)
		operacion=Porcentaje(CompA,CompB)
		operacion.print_result()
		Rebobinar()
	elif opc == "9":
		a = input("ingrese el valor a cambiar a decimal: ")
		operacion = DecimalToFraccion(a)
		operacion.print_result()
		Rebobinar()
	else:
		print("Error en la opcion ingresado")
		main()


print("-------------------Calculadora Basica-------------------")
main()
