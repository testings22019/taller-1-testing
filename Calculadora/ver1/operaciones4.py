def VerificaParentesis(exp):
    count = 0
    for i in exp:
        if i == "(":
            count += 1
        elif i == ")":
            count -= 1
        if count < 0:
            return False
    return count == 0

def minimo (denom, nume):
    denom = int(denom)
    nume = int(nume)
    flag = True
    divisor = 2
    while(flag == True):
        if denom % divisor == 0 and nume % divisor == 0:
            denom = denom / divisor
            nume = nume / divisor
            divisor = 2
        elif ((nume > divisor and denom == divisor) or (nume < divisor and denom == divisor)):
            flag = False
        else:
            divisor += 1
    exp = str(nume) + "/" + str(denom)
    return exp

def toStr(num):
    aux = str(num)
    if num >= 1:
        nume = ""
        count = 0
        for i in aux:
            if i != ".":
                nume = nume + str(i)
                count += 1
        denom = str(1)
        for i in range(1,count):
            denom = denom + str(0)
        exp = minimo(denom, nume)
    else:
        nume = ""
        count = 0 
        for i in aux:
            count += 1
            if i != "." and i != "0":
                nume = nume + str(i)                
        denom = str(1)
        for i in range(1,count-1):
            denom = denom + str(0)
        exp = minimo(denom,nume)        
    return exp

class DecimalToFraccion:
    def __init__(self, a):
        self.valor1 = float(a)
    def print_result(self):
        imp = toStr(self.valor1)
        print("El resultado es: ",imp)

