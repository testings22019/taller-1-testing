class Porcentaje:
    def __init__(self, a, b):
        self.valor1 = int(a)
        self.valor2 = int(b)
    def print_result(self):
        resultado = ((self.valor1/self.valor2)*100)
        print("El resultado es: ",resultado,"%")

class Raiz:
    def __init__(self, a):
        self.valor1 = int(a)
    def print_result(self):
        resultado = self.valor1**(1/2)
        print("El resultado es: ",resultado)


class RaizEnecima:
    def __init__(self, a, b):
        self.valor1 = int(a)
        self.valor2 = int(b)
    def print_result(self):
        resultado = self.valor1**(1/self.valor2)
        print("El resultado es: ",resultado)

class Potencia:
    def __init__(self, a, b):
        self.valor1 = int(a)
        self.valor2 = int(b)
    def print_result(self):
        resultado = self.valor1**self.valor2
        print("El resultado es: ",resultado)
